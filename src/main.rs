#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
}

enum Coin {
    Penny, 
    Nickle,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickle => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}!", state);
            25
        }
    }
}


fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }

}

fn main() {
    value_in_cents(Coin::Quarter(UsState::Alabama));
    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);

}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_value_in_cents() {
        let val = value_in_cents(Coin::Penny);
        assert_eq!(val, 1);

        let val = value_in_cents(Coin::Quarter(UsState::Alabama));
        assert_eq!(val, 25);

        let val = value_in_cents(Coin::Nickle);
        assert_eq!(val, 5);
    }

    #[test]
    fn test_plus_one() {
        let five = Some(5);
        let six = plus_one(five);
        assert_eq!(six.unwrap(), 6);
        let none = plus_one(None);
        assert_eq!(none, None);
    }
}